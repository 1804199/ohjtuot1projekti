﻿DROP DATABASE IF EXISTS Mokkivaraukset;
CREATE DATABASE Mokkivaraukset;

USE mokkivaraukset;
CREATE TABLE Asiakas
(
	Asiakasnumero INT(9) NOT NULL,
	Sukunimi VARCHAR(25),
	Etunimi VARCHAR(25),
	Puhelinnumero VARCHAR(13),
	Katuosoite VARCHAR(25),
	Paikkakunta VARCHAR(25),
	Postinumero INT(5),
	Maa VARCHAR(25),
	PRIMARY KEY (Asiakasnumero)
);

CREATE TABLE Varaus
(
	VarausID INT(9) NOT NULL AUTO_INCREMENT,
	Varaamis_pvm DATE,
	Alkaa DATE,
	Loppuu DATE,
	Voimassa BIT NOT NULL DEFAULT 1,
	Asiakasnumero INT,
	PRIMARY KEY (VarausID),
	FOREIGN KEY (Asiakasnumero) REFERENCES asiakas(Asiakasnumero)
);

CREATE TABLE Toimipiste
(
	ToimipisteID INT(9) NOT NULL,
	Katuosoite VARCHAR(25),
	Paikkakunta VARCHAR(25),
	Postinumero INT(5),
	Koordinaatit VARCHAR(35),
	PRIMARY KEY(ToimipisteID)
);

CREATE TABLE Lisapalvelu
(
	LisapalveluID INT(9) NOT NULL,
	Palvelunimi VARCHAR(25),
	Alkaa DATE,
	Paattyy DATE,
	Hinta DECIMAL(10,2) NOT NULL DEFAULT 0,
	Osallistujakapasiteetti INT(4),
	Lisatieto MEDIUMTEXT,
	ToimipisteID INT,
	PRIMARY KEY(LisapalveluID),
	FOREIGN KEY (ToimipisteID) REFERENCES Toimipiste(ToimipisteID)
);

CREATE TABLE Mokki
(
	MokkiID INT(9) NOT NULL,
	Katuosoite VARCHAR(25),
	Paikkakunta VARCHAR(25),
	Postinumero INT(5),
	Nimi VARCHAR(25),
	Koordinaatit VARCHAR(35),
	Kapasiteetti INT(4),
	Hinta DECIMAL(10,2) NOT NULL DEFAULT 0,
	ToimipisteID INT,
	PRIMARY KEY(MokkiID),
	FOREIGN KEY(ToimipisteID) REFERENCES toimipiste(ToimipisteID)
);

CREATE TABLE Palvelu
(
	PalveluID INT(9) NOT NULL,
	Hinta DECIMAL(10,2) NOT NULL DEFAULT 0,
	VarausID INT,
	LisapalveluID INT,
	MokkiID INT,
	PRIMARY KEY (PalveluID),
	FOREIGN KEY (VarausID) REFERENCES Varaus(VarausID),
	FOREIGN KEY (LisapalveluID) REFERENCES lisapalvelu(LisapalveluID),
	FOREIGN KEY (MokkiID) REFERENCES mokki(MokkiID)
);

CREATE TABLE Lasku
(
	LaskuID INT(9) NOT NULL AUTO_INCREMENT,
	Viitenumero VARCHAR(21),
	Erapaiva DATE,
	Laskutuspaiva DATE,
	Hinta DECIMAL(10,2) NOT NULL DEFAULT 0,
	Paperilasku BIT NOT NULL,
	PalveluID INT,
	PRIMARY KEY(LaskuID),
	FOREIGN KEY (PalveluID) REFERENCES Palvelu(PalveluID)
);