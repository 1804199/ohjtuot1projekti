INSERT INTO  Asiakas(Asiakasnumero, Sukunimi, Etunimi, Puhelinnumero, Katuosoite, Paikkakunta, Postinumero, Maa)
VALUES (1, 'Sulkkinen', 'Eetu', '+358503382971', 'Paavontie 3', 'Pirkkala', 33960, 'Suomi'); 
INSERT INTO  Asiakas(Asiakasnumero, Sukunimi, Etunimi, Puhelinnumero, Katuosoite, Paikkakunta, Postinumero, Maa)
VALUES (2, 'Oja', 'Maria', '+358508921143', 'Miilukatu 8', 'Kotka', 48200, 'Suomi');
INSERT INTO  Asiakas(Asiakasnumero, Sukunimi, Etunimi, Puhelinnumero, Katuosoite, Paikkakunta, Postinumero, Maa)
VALUES (3, 'Keltto', 'Ronja', '+358440391172', 'Niittykatu 16', 'Turku', 20720, 'Suomi');
INSERT INTO  Asiakas(Asiakasnumero, Sukunimi, Etunimi, Puhelinnumero, Katuosoite, Paikkakunta, Postinumero, Maa)
VALUES (4, 'Berglund', 'Olivia', '+46717093126', 'Folkskolegatan 4', 'Tukholma', 11735, 'Ruotsi');
INSERT INTO  Asiakas(Asiakasnumero, Sukunimi, Etunimi, Puhelinnumero, Katuosoite, Paikkakunta, Postinumero, Maa)
VALUES (5, 'Donner', 'Baldric', '+49400328471', 'Reutal 56', 'Shul', 98527, 'Saksa');
-- select * from asiakas

INSERT INTO  Varaus(VarausID, Varaamis_pvm, Alkaa, Loppuu, Voimassa, Asiakasnumero)
VALUES (null, '2019-01-05', '2019-04-18', '2019-04-23', 1, 3); 
INSERT INTO  Varaus(VarausID, Varaamis_pvm, Alkaa, Loppuu, Voimassa, Asiakasnumero)
VALUES (null, '2019-03-21', '2019-05-20', '2019-05-26', 0, 5);
INSERT INTO  Varaus(VarausID, Varaamis_pvm, Alkaa, Loppuu, Voimassa, Asiakasnumero)
VALUES (null, '2019-05-01', '2019-06-10', '2019-06-17', 1, 2);
INSERT INTO  Varaus(VarausID, Varaamis_pvm, Alkaa, Loppuu, Voimassa, Asiakasnumero)
VALUES (null, '2019-02-24', '2019-06-13', '2019-06-23', 1, 4);
INSERT INTO  Varaus(VarausID, Varaamis_pvm, Alkaa, Loppuu, Voimassa, Asiakasnumero)
VALUES (null, '2019-05-16', '2019-08-02', '2019-08-10', 1, 1);
-- select * from varaus

INSERT INTO  Toimipiste(ToimipisteID, Katuosoite, Paikkakunta, Postinumero, Koordinaatit)
VALUES (1, 'Salmentie 100b', 'Vihti', 43300, '60.374324,24.505545');
INSERT INTO  Toimipiste(ToimipisteID, Katuosoite, Paikkakunta, Postinumero, Koordinaatit)
VALUES (2, 'Urho Kekkosen katu 1', 'Helsinki', 30029, '60.169820,24.932967');
INSERT INTO  Toimipiste(ToimipisteID, Katuosoite, Paikkakunta, Postinumero, Koordinaatit)
VALUES (3, 'Toimelantie 19', 'Tampere', 33560, '61.524093,23.842441');
INSERT INTO  Toimipiste(ToimipisteID, Katuosoite, Paikkakunta, Postinumero, Koordinaatit)
VALUES (4, 'Karkullanvagen 1', 'Pargas', 21610, '60.348055,22.360659');
INSERT INTO  Toimipiste(ToimipisteID, Katuosoite, Paikkakunta, Postinumero, Koordinaatit)
VALUES (5, 'Laidunpolku 36', 'Naantali', 21100, '60.469607,22.050673');
-- select * from toimipiste

INSERT INTO  Lisapalvelu(LisapalveluID, Palvelunimi, Alkaa, Paattyy, Hinta, Osallistujakapasiteetti, Lisatieto, ToimipisteID)
VALUES (1, 'Opastettu luontoretki', '2019-06-01', '2019-07-31', 5.50, 15, 'Opastettu luontoretki Nuuksion kansallispuistossa. Retkin kesto on 2h ja 30min. Retkelle tuodaan omat eväät mukaan. Hinta on 5euroa per henkilo', 1); 
INSERT INTO  Lisapalvelu(LisapalveluID, Palvelunimi, Alkaa, Paattyy, Hinta, Osallistujakapasiteetti, Lisatieto, ToimipisteID)
VALUES (2, 'Kaupunkikierros', '2019-05-01', '2019-09-30', 10, 20, 'Helsingin kaupunki kierros bussilla. Varaudu hieman kävelyynkin. Hinta on 10euroa per henkilo', 2);
INSERT INTO  Lisapalvelu(LisapalveluID, Palvelunimi, Alkaa, Paattyy, Hinta, Osallistujakapasiteetti, Lisatieto, ToimipisteID)
VALUES (3, 'Yksityinen saunamokki', '2019-06-01', '2019-07-31', 15, 6, 'Kuuden hengen suanamokki rannassa. Hinta on 15euroa tunnilta', 3);
INSERT INTO  Lisapalvelu(LisapalveluID, Palvelunimi, Alkaa, Paattyy, Hinta, Osallistujakapasiteetti, Lisatieto, ToimipisteID)
VALUES (4, 'Maasto pyoraily', '2019-06-01', '2019-07-31', 10, 6, 'Sisältyy pyörät, kypärät ja kartta. Hinta on 10euroa kolme tuntia.', 4);
INSERT INTO  Lisapalvelu(LisapalveluID, Palvelunimi, Alkaa, Paattyy, Hinta, Osallistujakapasiteetti, Lisatieto, ToimipisteID)
VALUES (5, 'Kalliokiipeily', '2019-06-01', '2019-07-31', 25, 5, 'Kalliokiipeilya avustettujen ammattilaisten kanssa. Hinta on 25euroa per henkilo ja kiipeily kestää 2h', 5);        
-- select * from lisapalvelu

INSERT INTO  Mokki(MokkiID, Katuosoite, Paikkakunta, Postinumero, Nimi, Koordinaatit, Kapasiteetti, Hinta, ToimipisteID)
VALUES (1, 'Kirkkojarventie 14', 'Vihti', 33400, 'Keltainen', '60.411902,24.318168', 3, 70, 1);
INSERT INTO  Mokki(MokkiID, Katuosoite, Paikkakunta, Postinumero, Nimi, Koordinaatit, Kapasiteetti, Hinta, ToimipisteID)
VALUES (2, 'Vedakarrintie 35', 'Kirkkonummi', 22420, 'Karhu', '60.078052,24.607097', 4, 150, 2); 
INSERT INTO  Mokki(MokkiID, Katuosoite, Paikkakunta, Postinumero, Nimi, Koordinaatit, Kapasiteetti, Hinta, ToimipisteID)
VALUES (3, 'Vasamantie 23', 'Tampere', 33450, 'Mansikka', '61.560024,23.678277', 5, 100, 3);
INSERT INTO  Mokki(MokkiID, Katuosoite, Paikkakunta, Postinumero, Nimi, Koordinaatit, Kapasiteetti, Hinta, ToimipisteID)
VALUES (4, 'Klapparbergsvagen 4', 'Pragas', 39081, 'Graniitti', '60.295356,22.310476', 4, 85, 4);
INSERT INTO  Mokki(MokkiID, Katuosoite, Paikkakunta, Postinumero, Nimi, Koordinaatit, Kapasiteetti, Hinta, ToimipisteID)
VALUES (5, 'Rusthollinkatu 8', 'Naantali', 20880, 'Korppi', '60,415886,22.63515', 5, 90, 5); 
-- select * from Mokki

INSERT INTO  Palvelu(PalveluID, Hinta, VarausID, LisapalveluID, MokkiID)
VALUES (1, 100, 1, null, 1);
INSERT INTO  Palvelu(PalveluID, Hinta, VarausID, LisapalveluID, MokkiID)
VALUES (2, 180, 2, 1, 2);
INSERT INTO  Palvelu(PalveluID, Hinta, VarausID, LisapalveluID, MokkiID)
VALUES (3, 130, 3, 4, 3);
INSERT INTO  Palvelu(PalveluID, Hinta, VarausID, LisapalveluID, MokkiID)
VALUES (4, 115, 4, null, 4);
INSERT INTO  Palvelu(PalveluID, Hinta, VarausID, LisapalveluID, MokkiID)
VALUES (5, 120, 5, 5, 5);
-- select * from palvelu

INSERT INTO  Lasku(Viitenumero, Erapaiva, Laskutuspaiva, Hinta, Paperilasku, PalveluID)
VALUES (36411137586961231683, '2019-05-23', '2019-04-23', 380, 1, 1);
INSERT INTO  Lasku(Viitenumero, Erapaiva, Laskutuspaiva, Hinta, Paperilasku, PalveluID)
VALUES (29684636826886563587, '2019-06-26', '2019-05-26', 946.50, 1, 2);
INSERT INTO  Lasku(Viitenumero, Erapaiva, Laskutuspaiva, Hinta, Paperilasku, PalveluID)
VALUES (11578694133419845532, '2019-07-17', '2019-06-17', 770, 0, 3); 
INSERT INTO  Lasku(Viitenumero, Erapaiva, Laskutuspaiva, Hinta, Paperilasku, PalveluID)
VALUES (13747443549735976218, '2019-07-23', '2019-06-23', 880, 1, 4);
INSERT INTO  Lasku(Viitenumero, Erapaiva, Laskutuspaiva, Hinta, Paperilasku, PalveluID)
VALUES (21379985136787546577, '2019-09-10' , '2019-08-10', 800, 1, 5);
-- select * from lasku
